
import subprocess
import logging
import click
import os
import re
import pprint 

from flask.cli import with_appcontext

logger = logging.getLogger('webapp.dfs')

def _syscom(cmd, read_output=True, shell=False):
    """
        Execute a process
    """
    if type(cmd) == type([]):
        logger.debug("Syscom: %s" % subprocess.list2cmdline(cmd))
    else:
        logger.debug("Syscom: %s" % cmd)

    if not read_output:
        subprocess.Popen(cmd, shell=shell)
    else:
        p = subprocess.Popen(cmd, shell=shell, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        out, _ = p.communicate()
        out_list = out.splitlines()
        out_list2 = map(lambda x: x.decode('utf-8') if isinstance(x, (bytes, bytearray)) else x, out_list)
        #logger.debug("Output: %s" % out_list)
        #logger.debug("Output: %s" % list(out_list2))
        return out, out_list2



def _get_key(val, my_dict):
    """
        Returns a key in case the value is in the hash, null otherwise
    """ 
    for key, value in my_dict.items(): 
         if val == value: 
             return key 
    return None

def _do_job(dir, alldirs = {}):
    """
        Looks for all the directories under dir and calls for icalcs
    """
    dirs = filter(lambda x: os.path.isdir(os.path.join(dir, x)), os.listdir(dir))
    countdirs = 0
    for dirname in dirs:
        num, arr = _syscom(['ICACLS', os.path.join(dir, dirname)])
        # print("out_list2 is {} ".format(list(arr)))
        countdirs += 1
        permarr = []
        lastwo = None
        for item in list(arr):
            if not item.strip():
                continue
            if re.match(r'^G:.*\s.*', item, re.I):
                items = item.split()
                #print(items)
                #print(items[0])
                
                lastwo = os.path.normpath(items[0]).split(os.sep)[-2:]
                lastwo = '-'.join(lastwo)
                
                permarr = [items[1].lower()]
                #print(permarr)
            elif re.match(r'^successfully processed \d+ files; failed processing \d+ files', item, re.I|re.M):
                items = item.split()
                if int(items[-2]) > 0:
                    logger.debug('Some error retrieving permissions for {}'.format(os.path.join(dir, dirname)))
                continue
            else:
                permarr.append(item.strip().lower())
        newset = set(permarr)
        key = _get_key(newset, alldirs)
        if key:
            newkey = ','.join([key, lastwo])
            alldirs.pop(key)
            alldirs[newkey] = newset
        else:
            alldirs[lastwo] = newset

    print(alldirs)
    print('at {} we have processed {} directories, different permissions set found: {}'.format(dir, countdirs, len(alldirs.keys())))
    return alldirs

def _get_singles(alldirs):
    """
        Gets a frequency of individual members of ACL's
    """

    single_acl = {}
    for k,v in alldirs.items():
        for item in v:
            arr = item.split(':')
            value = arr[0].strip()
            if value in single_acl:
                single_acl[value] = single_acl[value] + 1
            else:
                single_acl[value] = 1
    return single_acl 


@click.command()
@click.option("--dry", help='Just output operations without doing it', is_flag=True)
@click.option("--dir", help='directory to obtain folders to check, we will only go one level below this directory')
@click.option("--single", help='get single components frequency in final ACL', is_flag=True)
@with_appcontext
def list_dfs_permissions(dry, dir, single):
    """
    list NTFS permissions of folders below this dir
    """
    logger.info("Starting script")
    dirs = []
    
    if not os.path.isdir(dir):
        logger.error("A directory: {} needs to be provided.".format(dir))
        return 1
    
    logger.info("Working on: <{}> .".format(dir))
    alldirs = _do_job(dir)
    if single and alldirs:
        all_singles = _get_singles(alldirs)
        sort_orders = sorted(all_singles.items(), key=lambda x: x[1], reverse=True)
        #print([len(x) for x in all_singles.keys()])
        pprint.pprint(sort_orders)
        res = sum(len(x) > 13 and not re.match(r'^s-',x) for x in all_singles.keys()) 
        print("Possible out of {}, {} are e-groups.".format(len(all_singles.keys()), res))

@click.command()
@click.option("--dry", help='Just output operations without doing it', is_flag=True)
@click.option("--dir", help='directory to obtain folders to check, we will only go one level below this directory')
@click.option("--single", help='get single components frequency in final ACL', is_flag=True)
@with_appcontext
def list_dfs_permissions_root(dry, dir, single):
    """
    list NTFS permissions of folders below this dir
    """
    logger.info("Starting script")
    dirs = []
    
    if not os.path.isdir(dir):
        logger.error("A directory: {} needs to be provided.".format(dir))
        return 1
    
    logger.info("Working on: <{}> .".format(dir))    
    if not os.path.isdir(dir):
        logger.error("A directory: {} needs to be provided.".format(dir))
        return 1    

    dirs = filter(lambda x: os.path.isdir(os.path.join(dir, x)) and re.match(r'^\d{4}$', x), os.listdir(dir))
    alldirs = {}
    for item in dirs:
        _do_job(os.path.join(dir,item), alldirs)

    if single and alldirs:
        all_singles = _get_singles(alldirs)
        sort_orders = sorted(all_singles.items(), key=lambda x: x[1], reverse=True)
        #print([len(x) for x in all_singles.keys()])
        pprint.pprint(sort_orders)
        res = sum(len(x) > 13 and not re.match(r'^s-',x) for x in all_singles.keys()) 
        print("Possible out of {}, {} are e-groups.".format(len(all_singles.keys()), res))
