import logging

from flask import Flask

from services.indico import list_dfs_permissions, list_dfs_permissions_root
from utils.logger import setup_logs

app = Flask(__name__)

app.config['LOG_LEVEL'] = 'DEV'
setup_logs(app, 'webapp', to_file=True)
logger = logging.getLogger('webapp')


@app.route('/')
def hello_world():
    return 'Hello, World!'


#
# Add the Command Line commands
#
app.cli.add_command(list_dfs_permissions)
app.cli.add_command(list_dfs_permissions_root)